#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -eo pipefail

display_help() {
    echo "Usage: $(basename "$0") InstallName"
    echo "                        Update docker images used by the Protective Node"
    echo "                        The image tags pulled remain the same as those configured for the current install"
    echo "                        The node is stopped before the update and started after the update"
    echo "       InstallName      A name made up of letters and/or digits and/or underscores only (mandatory)"
    echo "                        Identifies the folder contain the install to be updated"
    exit 1
}

# Script's args.
INSTALL_NAME="${1}"

if [ $# -eq 0 ]
then
    display_help
fi

if [[ "$INSTALL_NAME" =~ ^[0-9a-zA-Z_]+$ ]]
then
    echo "InstallName = $INSTALL_NAME"
else
    echo "ERROR: '$INSTALL_NAME' contains unexpected characters"
    display_help
fi

script_dir=`dirname $0`
install_dir="${INSTALL_NAME}"

if [ ! -f "${install_dir}/docker-compose.yml" ] 
then
    echo "'$INSTALL_NAME' is not a proper installation of Protective Node"
    exit 1
fi

cd $install_dir

# Stop Protective Node.
./stop.sh

# Get all images to be used by install.
if [[ $(uname -r) =~ Microsoft$ ]]
then
    docker-compose.exe pull
else
    docker-compose pull
fi

# Start Protective Node.
./start.sh
