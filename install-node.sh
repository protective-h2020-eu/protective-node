#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -e

display_help() {
    echo "Usage: $(basename "$0") ConfigName InstallName"
    echo "       ConfigName       A name made up of letters and/or digits and/or underscores only (mandatory)"
    echo "                        Identifies the folder containing configuration or a previous install"
    echo "       InstallName      A name made up of letters and/or digits and/or underscores only (mandatory)"
    echo "                        Identifies the folder to contain the new install"
    exit 1
}


# Script's args.
CONFIG_NAME="${1}"
INSTALL_NAME="${2}"

if [ $# -eq 0 ]
then
    display_help
fi

if [[ "$CONFIG_NAME" =~ ^[0-9a-zA-Z_]+$ ]]
then
    echo "ConfigName = $CONFIG_NAME"
else
    echo "ERROR: '$CONFIG_NAME' contains unexpected characters"
    display_help
fi

if [[ "$INSTALL_NAME" =~ ^[0-9a-zA-Z_]+$ ]]
then
    echo "InstallName = $INSTALL_NAME"
else
    echo "ERROR: '$INSTALL_NAME' contains unexpected characters"
    display_help
fi
source ./${CONFIG_NAME}/.env
script_dir=`dirname $0`
config_dir="${CONFIG_NAME}"
install_dir="${INSTALL_NAME}"

node_files="${CONFIG_PATH}/installation-files"

if [ ! -d "$config_dir" ] 
then
    echo "'$config_dir' does not exist, exiting installer"
    exit 1
fi

if [ ! -f "${config_dir}/docker-compose.yml" ] 
then
    echo "Configuring new node in '$INSTALL_NAME' from configuration in '$CONFIG_NAME'"
    refresh_node=do_not_refresh_node
else
    echo "Upgrading node from '$CONFIG_NAME' to '$INSTALL_NAME'"
    refresh_node=refresh_node
fi

if [ -d "$install_dir" ] 
then
    echo "'$install_dir' already exists, exiting installer"
    exit 1
fi
if [ -f "$install_dir" ] 
then
    echo "'$install_dir' already exists, exiting installer"
    exit 1
fi

cp -r $node_files $install_dir
cd $install_dir

if [ "$refresh_node" == "refresh_node" ]
then
    old_install_dir=$config_dir
    if [ -d "$old_install_dir" ]
    then
        ${old_install_dir}/stop-node.sh
        cp ${old_install_dir}/.env .env
        cp -r ${old_install_dir}/data/keys data/keys

        mv ${old_install_dir}/data/idea_files data/idea_files
        mv ${old_install_dir}/data/logs data/logs
        mv ${old_install_dir}/data/stats data/stats
        mv ${old_install_dir}/data/warden data/warden
    fi
else
    if [ -d "../$config_dir" ]
    then
        cp ../${config_dir}/.env .env
        cp -r ../${config_dir}/data/keys data/keys
    fi
fi

# Prepare ProtDash requests for HTTPS if needed
if [ "$HTTPS_ENABLED" = "true" ]
    then
       sed -i 's,:${HOST_PORT_FOR_DASHCONF},/persist,g' docker-compose.yml
       sed -i 's,:${HOST_PORT_FOR_NEON},,g' docker-compose.yml
       sed -i 's,:${HOST_PORT_FOR_MAIR},/camair,g' docker-compose.yml
       sed -i 's,:${HOST_PORT_FOR_KEYCLOAK},,g' docker-compose.yml
       sed -i 's,:${HOST_PORT_FOR_CRITERIA},/root,g' docker-compose.yml
       sed -i 's,\,http:,\,https:,g' docker-compose.yml
       sed -i 's,http://${WARDEN_SERVER_NAME}:8081/auth,https://${WARDEN_SERVER_NAME}/auth,g' docker-compose.yml
fi

# Get all images to be used by install.
if [[ $(uname -r) =~ Microsoft$ ]]
then
    docker-compose.exe pull
else
    docker-compose pull
fi

# Start Protective Node.
./start.sh
