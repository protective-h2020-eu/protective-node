#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -e

display_help() {
    echo "Usage: $(basename "$0") InstallName"
    echo "       InstallName      A name made up of letters and/or digits and/or underscores only (mandatory)"
    echo "                        Identifies the folder containing an existing installation of Protective Node"
    exit 1
}

# Script's args.
INSTALL_NAME="${1}"

if [ $# -eq 0 ]
then
    display_help
fi

if [[ "$INSTALL_NAME" =~ ^[0-9a-zA-Z_]+$ ]]
then
    echo "InstallName = $INSTALL_NAME"
else
    echo "ERROR: '$INSTALL_NAME' contains unexpected characters"
    display_help
fi

install_dir="${INSTALL_NAME}"

$install_dir/stop.sh
