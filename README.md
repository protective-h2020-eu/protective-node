Installing  Protective Node
======================================

This repository holds all that is neccessary to install and configure a **Protective Node** on a specific machine or VM. 

To start with the installation and configuration visit the [Wiki](https://gitlab.com/protective-h2020-eu/protective-node/wikis/home) of the project.