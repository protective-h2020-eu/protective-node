#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
# set -e

script_dir=`dirname $0`
cd $script_dir

# Stop local receiver for each remote Warden Server.
for remote_receiver_yml in *-receiver.yml
do
    if [ "$remote_receiver_yml" != "internal-receiver.yml" ]
    then
        if [[ $(uname -r) =~ Microsoft$ ]]
        then
            docker-compose.exe -f $remote_receiver_yml down
        else
            docker-compose -f $remote_receiver_yml down
        fi
    fi
done

# Stop https module
docker-compose -f https-module.yml down

# Stop core of Protective Node.
if [[ $(uname -r) =~ Microsoft$ ]]
then
    docker-compose.exe down
else
    docker-compose down
fi
