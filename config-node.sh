#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -eo pipefail

display_help() {
    echo "Usage: $(basename "$0") ConfigName"
    echo "       ConfigName       A name made up of letters and/or digits and/or underscores only (mandatory)"
    echo "                        Identifies the folder containing configuration or a previous install"
    exit 1
}

# Script's args.
CONFIG_NAME="${1}"

if [ $# -eq 0 ]
then
    display_help
fi

if [[ "$CONFIG_NAME" =~ ^[0-9a-zA-Z_]+$ ]]
then
    echo "ConfigName = $CONFIG_NAME"
else
    echo "ERROR: '$CONFIG_NAME' contains unexpected characters"
    display_help
fi


# Selection of the deployment mode
PS3='Please select the deployment mode that you want to install or reconfigure: '
options=("Peer-2-Peer mode -> Node" "Centralized Warden mode -> Central Warden Server" "Centralized Warden mode -> Node" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Peer-2-Peer mode -> Node")
            echo "You chose choice $REPLY which is $opt"
            CONFIG_PATH="P2P"
            break
            ;;
        "Centralized Warden mode -> Central Warden Server")
            echo "you chose choice $REPLY which is $opt"
            CONFIG_PATH="CW/central"
            break
            ;;
        "Centralized Warden mode -> Node")
            echo "you chose choice $REPLY which is $opt"
            CONFIG_PATH="CW/node"
            break
            ;;
        "Quit")
            echo "Exiting configuration manager"
            exit 1
            ;;
        *) echo "invalid option $REPLY. Please, try again.";;
    esac
done

script_dir=`dirname $0`
if [ -f "${CONFIG_NAME}/.env" ]
then
    old_config="$(cat ${CONFIG_NAME}/.env | grep CONFIG_PATH | sed -e 's#.*=\(\)#\1#')"
    if [[ $old_config != $CONFIG_PATH ]]
    then
        echo -e "WARNING! Found an older configuration of a different Node type.\n - Your current deployment configuration is of type: $old_config \n - You are trying to configure a deployment of type: $CONFIG_PATH \n"
        read -p "Do you want to overwrite your old configuration? (This will remove the current config folder: ${CONFIG_NAME}) [YyNn]" -r
        if [[ $REPLY =~ ^[Yy]$ ]]
        then
            rm -r ${CONFIG_NAME}
        else
            echo "Exiting configuration manager"
            exit 1
        fi
    fi
fi

find ${script_dir}/${CONFIG_PATH} -maxdepth 1 -type f | xargs -I {} cp {} ${script_dir}
config_dir="${CONFIG_NAME}"
mkdir -p ${config_dir}

long_config_dir="$(cd "$(dirname "$config_dir")"; pwd)/$(basename "$config_dir")"
env_file="${long_config_dir}/.env"

if [ ! -f "$env_file" ]
then
    cp ${script_dir}/.env.dist $env_file
    echo "# Configuration for deployment mode ${CONFIG_PATH}" >> $env_file
    echo "CONFIG_PATH=${CONFIG_PATH}" >> $env_file
fi

# For local Warden Server, Warden RA and Internal certificates.
local_warden_server_dir=${long_config_dir}/data/keys/warden_server
local_warden_ra_dir=${long_config_dir}/data/keys/warden_ra
local_internal_receiver_dir=${long_config_dir}/data/keys/internal_receiver

mkdir -p $local_warden_server_dir
mkdir -p $local_warden_ra_dir
mkdir -p $local_internal_receiver_dir

read -p "Enter a short name to identify your node or company [lowercase letters only]: " -r
         company_name=`echo "$REPLY" | awk '{print tolower($0)}'`
if [[ "$OSTYPE" == "darwin"* ]]
   then
        sed -i "" "s|{{COMPANY_NAME}}|${company_name}|g" $env_file
   else
        sed -i "s|{{COMPANY_NAME}}|${company_name}|g" $env_file
fi

read -p "Configure NEW (or replacement) certificate for local Warden Server [YyNn]? " -r
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # Enable HTTPS or not
    read -p "Do you want to enable HTTPS access to the UI of the node? (You will need to enter a valid DNS name on the next question) [YyNn]: " -r
    if [[ $REPLY =~ ^[Yy]$ ]]
       then
           https_enabled="true"
           https_text="DNS name"
       else
           https_enabled="false"
           https_text="DNS name or IP Address"
    fi
    if [[ "$OSTYPE" == "darwin"* ]]
       then
           sed -i "" "s|{{HTTPS_ENABLED}}|${https_enabled}|g" $env_file
       else
           sed -i "s|{{HTTPS_ENABLED}}|${https_enabled}|g" $env_file
    fi

    read -p "Enter your $https_text: " -r
    if [[ $REPLY =~ ^.+$ ]]
    then
        warden_server_name=$REPLY
        warden_ra_name=$REPLY
	# Get all images to be used by install.
	if [[ $(uname -r) =~ Microsoft$ ]]
	then
	# Convert path from unix to windows
	win_local_warden_server_dir=`echo $local_warden_server_dir | sed "s|^/mnt/||" | sed "s|/|:/|"` 
	    docker.exe run -v "$win_local_warden_server_dir":/certs \
               -e CA_SUBJECT=$warden_server_name \
               -e SSL_SUBJECT=$warden_server_name \
               registry.gitlab.com/protective-h2020-eu/protective-create-certs:latest
	else
	    docker run -v $local_warden_server_dir:/certs \
               -e CA_SUBJECT=$warden_server_name \
               -e SSL_SUBJECT=$warden_server_name \
               registry.gitlab.com/protective-h2020-eu/protective-create-certs:latest
	fi
        if [[ "$OSTYPE" == "darwin"* ]]
        then
            sed -i "" "s|{{WARDEN_SERVER_NAME}}|${warden_server_name}|g" $env_file
            sed -i "" "s|{{WARDEN_RA_NAME}}|${warden_ra_name}|g" $env_file
        else
            sed -i "s|{{WARDEN_SERVER_NAME}}|${warden_server_name}|g" $env_file
            sed -i "s|{{WARDEN_RA_NAME}}|${warden_ra_name}|g" $env_file
        fi
    fi
    # Copy warden server certificates to Warden ra folder
    cp -r $local_warden_server_dir/. $local_warden_ra_dir
fi

# Configure Warden Server Auth
read -p "Configuring X509NameAuthenticator (production) by default, do you want to change it to PlainAuthenticator (development/debugging) [YyNn]?" -r
if [[ $REPLY =~ ^[Yy]$ ]]
   then
        warden_auth="PlainAuthenticator"
   else
        warden_auth="X509NameAuthenticator"
fi
if [[ "$OSTYPE" == "darwin"* ]]
   then
        sed -i "" "s|{{WARDEN_AUTHENTICATOR}}|${warden_auth}|g" $env_file
   else
        sed -i "s|{{WARDEN_AUTHENTICATOR}}|${warden_auth}|g" $env_file
fi

# Configure remote connections
if [[ $CONFIG_PATH == "P2P" ]]
    then
        # For each remote Warden Server.
        read -p "Configure connections to remote Warden Servers now [YyNn]? " -r
        #echo    # (optional) move to a new line
        while [[ $REPLY =~ ^[Yy]$ ]]
        do
            read -p "Enter a short name for a remote Warden Server (not INTERNAL) [letters and digits only]: " -r
            remote_warden_lc=`echo "$REPLY" | awk '{print tolower($0)}'`
            remote_warden_uc=`echo "$REPLY" | awk '{print toupper($0)}'`

            remote_receiver_dir=${long_config_dir}/data/keys/${remote_warden_lc}_receiver

            read -p "Do you want to receive Meta-alerts from this remote Warden Server [YyNn]? " -r
            if [[ $REPLY =~ ^[Yy]$ ]]
            then
                receive_metaalerts="true"
            else
                receive_metaalerts="false"
            fi

            if [ "$remote_warden_uc" == "INTERNAL" ]
            then
                echo "WARNING: ${remote_warden_uc} is reserved and should not be used, skipping configuring ${remote_warden_uc}!"
            else
                if grep -q "^${remote_warden_uc}_WARDEN_CLIENT_HOSTNAME=" $env_file
                then
                    echo "WARNING: ${remote_warden_uc}_WARDEN_CLIENT_HOSTNAME is already configured, skipping configuring ${remote_warden_uc}!"
                else
                    echo "# Support for ${remote_warden_uc}" >> $env_file
                    echo "${remote_warden_uc}_WARDEN_CLIENT_HOSTNAME=eg.${remote_warden_lc}.rx.warden.protective.org" >> $env_file
                    echo "${remote_warden_uc}_WARDEN_CLIENT_EMAIL=eg.some.name@warden.protective.org" >> $env_file
                    echo "${remote_warden_uc}_WARDEN_CLIENT_NAME=org.protective.warden.rx.${remote_warden_lc}.eg" >> $env_file
                    echo "${remote_warden_uc}_WARDEN_SERVER_URL=https://${remote_warden_lc}.warden.protective.org" >> $env_file
                    echo "RECEIVE_${remote_warden_uc}_METAALERTS=${receive_metaalerts}" >> $env_file

                    mkdir -p $remote_receiver_dir
                    echo -e "\033[1;33mIMPORTANT NOTE: You will need to copy the $remote_warden_uc key files into the $remote_receiver_dir"
                    echo -e "You should have these key files to identify this connection to the remote Warden Server.\033[0m"
                fi
            fi

            read -p "Configure another connection to a remote Warden Server [YyNn]? " -r
        done
fi
if [[ $CONFIG_PATH == "CW/node" ]]
    then
       mkdir -p ${long_config_dir}/data/keys/central_sender_receiver
fi

read -p "Edit the .env file to set required values for environment variables, press any key to continue. " -n 1 -r
echo    # (optional) move to a new line
nano $env_file
