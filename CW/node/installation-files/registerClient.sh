#!/bin/bash

# This script helps adding a new client into Warden Server, registering it into Warden RA and getting the token.

# List the clients
echo -e "Listing the current registered clients in Warden Server...\n"
docker exec -it protective_warden_1 python warden_server.py list
echo -e "\n"

# Get information for new client registration
read -p "Do you want to register a new client in Warden Server and generate the certificates for it? [YyNn] " -r
if [[ $REPLY =~ ^[Yy]$ ]]
    then
        while [[ $client_name == '' ]]
        do
            read -p "Enter the name of the client: " -r
            if [[ $REPLY ]]
                then
                    client_name=$REPLY
                else
                    echo -e "You need to provide a client name.\n"
            fi
        done
        while [[ $client_hostname == '' ]]
        do
            read -p "Enter the hostname of the client: " -r
            if [[ $REPLY ]]
                then
                    client_hostname=$REPLY
                else
                    echo -e "You need to provide a client hostname.\n"
            fi
        done
        while [[ $client_email == '' ]]
        do
            read -p "Enter the email address of the client: " -r
            if [[ $REPLY ]]
                then
                    client_email=$REPLY
                else
                    echo -e "You need to provide a client email address.\n"
            fi
        done
        # Register the client
        echo -e "Registering the new client in Warden Server with the information you provided...\n"
        docker exec protective_warden_1 python warden_server.py register --name $client_name --hostname $client_hostname --requestor $client_email --read --write --notest
        echo -e "Done!\n"

        # Register the client in Warden RA and generating token
        echo -e "Registering the new client in Warden RA...\n"
        docker exec protective_wardenra_1 python warden_ra.py register --name $client_name --admins $client_email
        echo -e "Done!\n"
        echo -e "Generating the token...\n"
        token=$(docker exec protective_wardenra_1 python warden_ra.py applicant --name $client_name | tail -n +5 | awk '{print $4}' );
        echo -e "The Generated token is: $token"
        #docker exec protective_wardenra_1 python warden_ra.py applicant --name $client_name
        echo -e "Done!\n"

        # Prepare the folder to send to the client
        mkdir $client_name
        cp data/keys/warden_server/server.crt $client_name
        cp ../warden_apply.sh $client_name
        warden_ra_URL=`grep WARDEN_RA_URL .env | sed -e 's#.*=\(\)#\1#'`
        echo "./warden_apply.sh --cacert server.crt $warden_ra_URL $client_name $token" > ./$client_name/generateCERTS.sh
        chmod +x ./$client_name/generateCERTS.sh

        # Generate the certs for the client
	cd $client_name
	./generateCERTS.sh
   else
        exit
fi


