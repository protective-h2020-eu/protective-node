#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -e

script_dir=`dirname $0`
cd $script_dir

# Start core of Protective Node.
if [[ $(uname -r) =~ Microsoft$ ]]
    then
        docker-compose.exe up -d
    else
        docker-compose up -d
fi

remote_sender_receiver_yml=central-sender-receiver.yml
remote_sender_receiver_dir=central_sender_receiver
https_yml=https-module.yml

receive_from_remote_line=`echo "CENTRAL_METAALERTS"`
receive_from_remote_val=`grep $receive_from_remote_line .env | sed -e 's#.*=\(\)#\1#'`

if [ "$receive_from_remote_val" = "true" ]
    then
        # Comment the line to receive the metaalerts.
        if [[ "$OSTYPE" == "darwin"* ]]
            then
                sed -i "" "s|\"nocat\"|#\"nocat\"|g" data/conf/$remote_sender_receiver_dir/warden_filer.cfg
            else
                sed -i "s|\"nocat\"|#\"nocat\"|g" data/conf/$remote_sender_receiver_dir/warden_filer.cfg
        fi
fi

if [ ! -d "data/keys/$remote_sender_receiver_dir" ]
    then
        mkdir -p data/keys/$remote_sender_receiver_dir
        echo "WARNING: No keys found for $remote_warden_uc"
        echo "-------- You may need to add keys to 'data/keys/$remote_sender_receiver_dir'"
fi

# Enable HTTPS component
https_line=`echo "HTTPS_ENABLED"`
https_val=`grep $https_line .env | sed -e 's#.*=\(\)#\1#'`

if [ "$https_val" = "true" ]
    then
        # Start docker with https component
	if [[ $(uname -r) =~ Microsoft$ ]]
            then
                docker-compose.exe -f $https_yml up -d
            else
                docker-compose -f $https_yml up -d
        fi
fi

# Start local receiver for the remote Warden Server.
if [[ $(uname -r) =~ Microsoft$ ]]
    then
        docker-compose.exe -f $remote_sender_receiver_yml up -d
    else
        docker-compose -f $remote_sender_receiver_yml up -d
fi

# Auto generate internal receiver certificates.
source .env

## INTERNAL RECEIVER & META-ALERT SENDER AUTOCERTIFICATE GENERATION
# Register and get the token for internal warden receiver client
if [ ! -f "data/keys/internal_receiver/cert.pem" ]
    then
	echo "Auto-generating internal certificates..."
	echo "Registering client in Warden RA and getting the token"
	docker exec -it protective_wardenra_1 python warden_ra.py register --name $INTERNAL_WARDEN_CLIENT_NAME --admins $INTERNAL_WARDEN_CLIENT_EMAIL;
	TOKEN=$(docker exec -it protective_wardenra_1 python warden_ra.py applicant --name $INTERNAL_WARDEN_CLIENT_NAME | tail -n +5 | awk '{print $4}' );
	TOKEN=${TOKEN%$'\r'}

	# Clean first
	key=key.pem
	csr=csr.pem
	cert=cert.pem
	for n in "$csr" "$key" "$cert"; do
	    [ -e "$n" ] && rm $n
	done

	# Generate the certificates
	echo "Warden RA URL: "$WARDEN_RA_URL
	echo "Client name: "$INTERNAL_WARDEN_CLIENT_NAME
	echo "Token: "$TOKEN

	echo "Generating certificates"
	../warden_apply.sh --cacert data/keys/warden_server/server.crt "$WARDEN_RA_URL" "$INTERNAL_WARDEN_CLIENT_NAME" "$TOKEN"

	# Move certificates to the correct location
	echo "Moving the certificates"
	mv csr.pem key.pem cert.pem data/keys/internal_receiver
	cp data/keys/warden_server/server.crt data/keys/internal_receiver

	# Restart the internal receiver and meta-alert sender
	echo "Restarting the containers to apply the changes"
	if [[ $(uname -r) =~ Microsoft$ ]]
	then
	   docker.exe restart protective_internalrx_1
	   docker.exe restart protective_meta-alert_sender_1
           docker.exe restart protective_local_receiver_to_central_1
	else
	   docker restart protective_internalrx_1
	   docker restart protective_meta-alert_sender_1
           docker restart protective_local_receiver_to_central_1
	fi
	echo "Internal certificates generation finished successfully"
    else
	echo "Internal certificates already exist, skipping their generation"
fi
## END INTERNAL RECEIVER & META-ALERT SENDER
## Register second internal receiver to share alerts with central warden
echo "Registering the receiver that will send the events to central"

RESULT=$(docker exec -it protective_warden_1 python warden_server.py list | grep receiver.to.central)
if [ -n "$RESULT" ]; then
    echo "Receiver already registered"
else
    docker exec -it protective_warden_1 python warden_server.py register --name receiver.to.central --hostname central.to.receiver --requestor internal2@mail.com --read --notest
fi

echo "Finished commands to start the PROTECTIVE Node"

