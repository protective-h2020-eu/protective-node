#!/bin/bash

# e = Exit immediately if a simple command exits with a non-zero status, unless...
# x = Print a trace of simple commands and their arguments after they areexpanded and before they are executed.
set -e

script_dir=`dirname $0`
cd $script_dir

# Start Central Warden components.
if [[ $(uname -r) =~ Microsoft$ ]]
then
    docker-compose.exe up -d
else
    docker-compose up -d
fi